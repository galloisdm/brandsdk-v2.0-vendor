# BrandSDK-V2.0

Update dist/json with the new brand.json file.

Data-brandcode is needed on the script that includes the js file. It's no longer needed on each form except as an overwrite. The following script should be placed after jQuery if it exists.

```html
<script src="/BrandSDK-v2.0/BrandSDK.min.js" data-brandcode="BRANDCODE" data-appkey="KEYHERE"></script>
```

There's a BrandSDK Configuration Tool to help build that script if needed. It can be accessed on the root of any site IE yoursite.com/BrandSDK-v2.0. Live example: [http://p.fbwineapps.com/BrandSDK-v2.0/](http://p.fbwineapps.com/BrandSDK-v2.0/)  


The BrandSDK script can be loaded in the footer, which is ideal for those concerned with SEO and load times, but if you do, you must wrap your callback functions in `BrandSDK.ready()`

## Age Gate (Optional)
-  Adding `data-agegate="1"` will use SDK logic for the age gate. By default it will look for a page in the root named `/agegate`. This can be overridden by adding an attribute of ` data-agegate-path="/new/path/to/agegate.php"`.
- If the age gate needs only a button to pass, add a class of `btn-enter` to the button.
- If the age gate requires a dob check then add the following names to the inputs: `day`, `month`, `year` OR use these id's: `dobDay`, `dobMonth`, `dobYear`. For the time being, all fields need to be wrapped in a div with the class `date-of-birth`.
- If you need to change the legal drinking age to something other than 21 you can add `data-agegate-age="18"`.
- Parsley is used for error handling. You can read their docs to learn more. `data-parsley-errors-messages-disabled` will turn off messages for an input. `data-parsley-required-message` will change the message that appears.
- The agegate will not show on legal pages as long as the path contains `/legal/` or the page has a body class of `brandsdk-legal-page`

## More Optional Attributes
- `data-disabled="1"` will prepare the BrandSDK but not initialize it. This is needed for JS apps like VUE and Angular. You can initialize when ready by using `window.BrandSDK.init();`
- `data-jquery="1"` can be used when you are developing a website that needs jquery, and you want to use BrandSDK's version. It will load jQuery and leave it exposed on the common `window.$` and `window.jQuery` properties
- `data-wtb-logo-path="/path-to-logo"` can be added to fix the image path for the **store finder**. Please use absolute urls or start with `/` if possible.

## Legal Pages

Legal pages should exist in `/legal/page-name` for consistency.

Content will automatically be populated by entering the following codes on each page.

- **privacy-policy**: `{bapi-privacy-policy}` OR `<div id="bapi-privacy-policy"></div>`
- **use-agreement**: `{bapi-terms-of-use}` OR `<div id="bapi-terms-of-use"></div>`
- **ca-privacy-notice**: `{bapi-ca-privacy-notice}` OR `<div id="bapi-ca-privacy-notice"></div>`
- **trademarks**: `{bapi-trademarks}` OR `<div id="bapi-trademarks"></div>`
  
The benefit of using the divs is we can put a loading indicator inside. 



## Debugging

We are not console logging anything in production. If you need to debug a live site, open the console and type `BrandSDK.log` to find anything that may have been logged. 

- - -

# Gallo Dev Notes:

## Building
Only need to build when updating anything in the src folder.

``` shell
npm run build
```


### REPLACEMENT NOTES
BrandSDK is looking for these tags and will be replaced with data from the brand's json files.
pbwineapps legal pages:
- <%contact-link%>
- <%unsubscribe-link%>
- <%privacy-policy%>
- <%ca-privacy-notice%>

contact form:
- {{brand_name}}
- {{phone}}
- {{winefinder}}
- {{privacy_policy}}
- {{brand_code}}

newsletter:
- {{brand_name}}
- {{brand_code}}
- {{privacy_policy}}

unsub:
- {{brand_name}}
- {{brand_code}}

wtb:
- {{brand_code}}
- {{brand_id}}


### wineos-storefront replacements
- 'site-name': this.config.Brand.BrandName,
- 'phone': this.config.Brand.BrandName, 
- 'email': 'info@' + this.config.Brand.Domain,
- 'unsubscribe-link': '<a href="/unsubscribe">Unsubscribe Link</a>',
- 'unsubscribe-href': '/unsubscribe',
- 'contact-link': '<a href="/contact">Contact Link</a>',
- 'contact-href': '/contact',