<footer class="bg-danger text-light p-3">
	<div class="container">
	<ul class="nav nav-pills nav-fill">
			<li class="nav-item">
				<a class="nav-link text-white" href="/legal/privacy-policy.php">Privacy Policy</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/legal/use-agreement.php">Use Agreement</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/legal/trademarks.php">Trademarks</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/legal/ca-privacy-notice.php">California Privacy Notice</a>
			</li>
		</ul>
	</div>
</footer>

<!-- If you wish to load the BrandSDK in the footer, you must wrap your callback functions in `BrandSDK.ready()` -->
<script src="/BrandSDK-v2.0/BrandSDK.min.js" data-brandcode="LAM" data-appkey="b2d6b1p9-d653-6d89-c04b-cbc2193241g4" data-agegate="1" data-agegate-path="/agegate.php" data-wtb-logo-path="/images/logo.png" data-jquery="1"></script>
<script src="/js/main.min.js"></script>
</body>
</html>
