<!doctype html>
<html lang='en'>

<head>
	<!-- Required meta tags -->
	<meta charset='utf-8'>
	<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>Sample Site</title>

</head>

<body>

<header class="bg-danger p-3">
	<div class="container">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item">
				<a class="nav-link text-white" href="/">Home</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/contact.php">Contact</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/locator.php">Store Locator</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/newsletter.php">Newsletter</a>
			</li>
			<li class="nav-item">
				<a class="nav-link text-white" href="/unsubscribe.php">Unsubscribe</a>
			</li>
		</ul>
	</div>
</header>