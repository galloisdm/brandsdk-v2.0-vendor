<?php include "partials/header.php" ?>

<div class="container my-5">
	<h1>Sample Site Using BrandSDK-v2.0</h1>
	<p>This site is using the following features of the BrandSDK</p>
	<ol>
		<li>jQuery</li>
		<li>Age Gate</li>
		<li>Legal Content</li>
		<li>Contact Form: with and without Salesforce</li>
		<li>Newsletter: with and without Salesforce</li>
		<li>Unsubscribe: with and without Salesforce</li>
		<li>Store Finder</li>
	</ol>
</div>

<?php include "partials/footer.php" ?>