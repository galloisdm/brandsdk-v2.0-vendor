<?php
/**
 * If this page were to contain full html, with a <head> etc, the BrandSDK would iframe the age gate. This is to prevent the common age gate loop
 */
?>

<div class="agegate-container d-flex flex-column justify-content-center align-items-center">


	<!-- Just an enter button -->
	<div class="simple my-3 bg-light p-5 border text-center">
		<h2>You must be some age to enter this site.</h2>
		<p>Are you that age?</p>
		<button class="btn btn-danger btn-enter">I am that age!</button>
	</div>


	<!--  Enter DOB -->
	<div class="my-3 w-100 bg-light p-5 border text-center">
		<p>You must be 21 I'm pretty sure. Enter your age below to pass the test.</p>
		<form>
			<div class="date-of-birth d-flex">

				<label for="month" class="sr-only">Month</label>
				<select class="dobfields custom-select select-month" name="month" id="month">
					<option selected value="">MM</option>
					<?php for ($m=1; $m <= 12; $m++): ?>
					<option value="<?= sprintf("%02d", $m) ?>"><?php echo sprintf("%02d", $m); ?></option>
					<?php endfor; ?>
				</select>

				<label for="day" class="sr-only">Day</label>
				<select class="dobfields custom-select select-day mx-2" name="day" id="day" data-parsley-required-message="Day is required">
					<option selected value="">DD</option>
					<?php for ($d=1; $d <= 31; $d++): ?>
					<option value="<?= sprintf("%02d", $d) ?>"><?php echo sprintf("%02d", $d); ?></option>
					<?php endfor; ?>
				</select>

				<label for="year" class="sr-only">Year</label>
				<select class="dobfields custom-select select-year flex-grow-1" name="year" id="year">
					<option selected value="">YYYY</option>
					<?php for ($y=date('Y'); $y >= 1900; $y--): ?>
					<option value="<?= $y ?>"><?php echo $y; ?></option>
					<?php endfor; ?>
				</select>

			</div>
			<button type="submit" name="submit" class="btn btn-danger align-self-center d-block my-3 mx-auto">
				Enter
			</button>
		</form>
	</div>


</div>