// If loading the BrandSDK in the footer, you need to wrap your callback functions in BrandSDK.ready();
BrandSDK.ready(function () {
	console.log('The BrandSDK is ready. Do your thing');

	// ejgContactUs_callback is a function in the BrandSDK
	var oldContactUsCallback = ejgContactUs_callback,
		// ejgWTB_callback is a function in the BrandSDK
		oldWTBCallback = ejgWTB_callback,
		// ejgNewsletter_callback is a function in the BrandSDK
		oldNewsletterCallback = ejgNewsletter_callback,
		// ejgUnsubscribe_Callback is a function in the BrandSDK
		oldUnsubscribeCallback = ejgUnsubscribe_callback;

	var wtbEle = $('.ejgWhereToBuy');
	if (wtbEle.length > 0) {
		// By overriding it below, the BrandSDK will execute that modified
		// function below when it reaches that function in the BrandSDk.
		ejgWTB_callback = function () {
			oldWTBCallback.apply(this);

			// Where to Buy form element object.
			var wtbForm = wtbEle.find('.form');

			// Modification starts below.
			wtbForm.find('label').addClass('mr-3');
			wtbForm.find('.group').removeClass('row').addClass('my-3');
			wtbForm.find('.wtbSubmit').addClass('btn btn-danger mb-3');

		};
	}

	var cEle = $('#ejgContactUs');
	if (cEle.length > 0) {
		// By overriding it below, the BrandSDK will execute that modified
		// function below when it reaches that function in the BrandSDk.

		ejgContactUs_callback = function () {

			// Where to Buy form element object.
			var cForm = cEle.find('.form');

			// Modification starts below.


		};
	}

	var nEle = $('#ejgNewsletter');
	if (nEle.length > 0) {
		// By overriding it below, the BrandSDK will execute that modified
		// function below when it reaches that function in the BrandSDk.
		ejgNewsletter_callback = function () {
			oldNewsletterCallback.apply(this);

			// Where to Buy form element object.
			var nForm = nEle.find('.form');

			// Modification starts below.
			nForm.find('label').addClass('mr-3');
			nForm.find('.row').removeClass('row').addClass('group my-3');
			nForm.find('input#submit').addClass("btn btn-danger mb-3");
		};
	}

	var unsubEle = $('#ejgUnsubscribe');
	if (unsubEle.length > 0) {
		// By overriding it below, the BrandSDK will execute that modified
		// function below when it reaches that function in the BrandSDk.
		ejgUnsubscribe_callback = function () {
			oldUnsubscribeCallback.apply(this);

			// Where to Buy form element object.
			var unsubForm = unsubEle.find('.form');

			// Modification starts below.
			unsubForm.find('label').addClass('mr-3');
			unsubForm.find('.row').removeClass('row').addClass('group my-3');
			unsubForm.find('input#submit').addClass("btn btn-danger mb-3");
		};
	}
})